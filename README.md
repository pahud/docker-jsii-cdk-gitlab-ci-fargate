# Docker image with JSII and AWS CDK for GitLab CI on Fargate

A Docker image ready to run JSII and AWS CDK CI jobs with the [AWS Fargate Custom
Executor driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate)
for [GitLab Runner](https://docs.gitlab.com/runner).

[![pipeline
status](https://gitlab.com/pahud/docker-jsii-cdk-gitlab-ci-fargate/badges/master/pipeline.svg)](https://gitlab.com/pahud/docker-jsii-cdk-gitlab-ci-fargate/-/commits/master)
